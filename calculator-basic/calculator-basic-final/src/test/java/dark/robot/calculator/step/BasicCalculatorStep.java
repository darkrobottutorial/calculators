package dark.robot.calculator.step;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dark.robot.calculator.basic.BasicCalculator;
import dark.robot.calculator.basic.ICalculator;

import static org.junit.Assert.assertEquals;

public class BasicCalculatorStep {

    private ICalculator calculator = BasicCalculator.getInstance();

    private Double left;
    private Double right;
    private Double result;

    @Given("^the \"([^\"]*)\" and \"([^\"]*)\" numbers$")
    public void the_and_numbers(Double left, Double right) throws Throwable {
        this.left = left;
        this.right = right;
    }

    @When("^use the calculator to add them$")
    public void the_calculator_to_add_them() throws Throwable {
        result = calculator.add(left, right);
    }

    @Then("^the add result is \"([^\"]*)\"$")
    public void the_add_result_is(Double result) throws Throwable {
        assertEquals(this.result, result);
    }

    @When("^use the calculator to subtract them$")
    public void the_calculator_to_subtract_them() throws Throwable {
        result = calculator.subtract(left, right);
    }

    @Then("^the subtraction result is \"([^\"]*)\"$")
    public void the_subtraction_result_is(Double result) throws Throwable {
        assertEquals(this.result, result);
    }

    @When("^use the calculator to divide them$")
    public void the_calculator_to_divide_them() throws Throwable {
        result = calculator.divide(left, right);
    }

    @Then("^the division result is \"([^\"]*)\"$")
    public void the_division_result_is(Double result) throws Throwable {
        assertEquals(this.result, result);
    }

    @When("^use the calculator to multiply them$")
    public void the_calculator_to_multiply_them() throws Throwable {
        result = calculator.multiply(left, right);
    }

    @Then("^the multiplication result is \"([^\"]*)\"$")
    public void the_multiplication_result_is(Double result) throws Throwable {
        assertEquals(this.result, result);
    }
}