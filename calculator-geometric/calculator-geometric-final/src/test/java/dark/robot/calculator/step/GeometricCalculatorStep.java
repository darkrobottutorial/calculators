package dark.robot.calculator.step;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dark.robot.calculator.geometric.GeometricCalculator;
import dark.robot.calculator.geometric.IGeometricCalculator;
import dark.robot.calculator.geometric.figure.Circle;
import dark.robot.calculator.geometric.figure.Figures;
import dark.robot.calculator.geometric.figure.Rectangle;
import dark.robot.calculator.geometric.figure.Square;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class GeometricCalculatorStep {

    private IGeometricCalculator calculator = GeometricCalculator.getInstance();
    private Map<String, Double> arguments = new HashMap<>();

    private Double radio;
    private Double diameter;
    private Double left;
    private Double right;
    private Double side;
    private Double diagonal;

    private Double result;

    @Given("^the \"([^\"]*)\" radio$")
    public void the_radio(Double radio) throws Throwable {
        this.radio = radio;
    }

    @Given("^the \"([^\"]*)\" diameter$")
    public void the_diameter(Double diameter) throws Throwable {
        this.diameter = diameter;
    }

    @When("^use the geometric calculator to get a circle area using circle radio$")
    public void use_the_geometric_calculator_to_get_a_circle_area_using_circle_radio() throws Throwable {
        arguments.put(Circle.Keys.radio.name(), radio);
        result = calculator.calculateArea(Figures.Circle.name(), arguments);
        arguments.clear();
    }

    @When("^use the geometric calculator to get a circle area using circle diameter$")
    public void use_the_geometric_calculator_to_get_a_circle_area_using_circle_diameter() throws Throwable {
        arguments.put(Circle.Keys.diameter.name(), diameter);
        result = calculator.calculateArea(Figures.Circle.name(), arguments);
        arguments.clear();
    }

    @When("^use the geometric calculator to get a circle perimeter using circle radio$")
    public void use_the_geometric_calculator_to_get_a_circle_perimeter_using_circle_radio() throws Throwable {
        arguments.put(Circle.Keys.radio.name(), radio);
        result = calculator.calculatePerimeter(Figures.Circle.name(), arguments);
        arguments.clear();
    }

    @When("^use the geometric calculator to get a circle perimeter using circle diameter$")
    public void use_the_geometric_calculator_to_get_a_circle_perimeter_using_circle_diameter() throws Throwable {
        arguments.put(Circle.Keys.diameter.name(), diameter);
        result = calculator.calculatePerimeter(Figures.Circle.name(), arguments);
        arguments.clear();
    }

    @Then("^the circle area is \"([^\"]*)\"$")
    public void the_circle_area_is(Double result) throws Throwable {
        assertEquals(result, this.result);
    }

    @Then("^the circle perimeter is \"([^\"]*)\"$")
    public void the_circle_perimeter_is(Double result) throws Throwable {
        assertEquals(result, this.result);
    }

    @Given("^the \"([^\"]*)\" and \"([^\"]*)\" sides$")
    public void the_and_sides(Double left, Double right) throws Throwable {
        this.left = left;
        this.right = right;
    }

    @When("^use the geometric calculator to get a rectangle area$")
    public void use_the_geometric_calculator_to_get_a_rectangle_area() throws Throwable {
        arguments.put(Rectangle.Keys.sideA.name(), left);
        arguments.put(Rectangle.Keys.sideB.name(), right);
        result = calculator.calculateArea(Figures.Rectangle.name(), arguments);
        arguments.clear();
    }

    @When("^use the geometric calculator to get a rectangle perimeter$")
    public void use_the_geometric_calculator_to_get_a_rectangle_perimeter() throws Throwable {
        arguments.put(Rectangle.Keys.sideA.name(), left);
        arguments.put(Rectangle.Keys.sideB.name(), right);
        result = calculator.calculatePerimeter(Figures.Rectangle.name(), arguments);
        arguments.clear();
    }

    @Then("^the rectangle area is \"([^\"]*)\"$")
    public void the_rectangle_area_is(Double result) throws Throwable {
        assertEquals(result, this.result);
    }

    @Then("^the rectangle perimeter is \"([^\"]*)\"$")
    public void the_rectangle_perimeter_is(Double result) throws Throwable {
        assertEquals(result, this.result);
    }

    @Given("^the \"([^\"]*)\" sides$")
    public void the_sides(Double side) throws Throwable {
        this.side = side;
    }

    @Given("^the \"([^\"]*)\" diagonal$")
    public void the_diagonal(Double diagonal) throws Throwable {
        this.diagonal = diagonal;
    }

    @When("^use the geometric calculator to get a square area using side$")
    public void use_the_geometric_calculator_to_get_a_square_area_using_side() throws Throwable {
        arguments.put(Square.Keys.side.name(), side);
        result = calculator.calculateArea(Figures.Square.name(), arguments);
        arguments.clear();
    }

    @When("^use the geometric calculator to get a square area using diagonal$")
    public void use_the_geometric_calculator_to_get_a_square_area_using_diagonal() throws Throwable {
        arguments.put(Square.Keys.diagonal.name(), diagonal);
        result = calculator.calculateArea(Figures.Square.name(), arguments);
        arguments.clear();
    }

    @When("^use the geometric calculator to get a square perimeter using side$")
    public void use_the_geometric_calculator_to_get_a_square_perimeter_using_side() throws Throwable {
        arguments.put(Square.Keys.side.name(), side);
        result = calculator.calculatePerimeter(Figures.Square.name(), arguments);
        arguments.clear();
    }

    @When("^use the geometric calculator to get a square perimeter using diagonal$")
    public void use_the_geometric_calculator_to_get_a_square_perimeter_using_diagonal() throws Throwable {
        arguments.put(Square.Keys.diagonal.name(), diagonal);
        result = calculator.calculatePerimeter(Figures.Square.name(), arguments);
        arguments.clear();
    }

    @Then("^the square area is \"([^\"]*)\"$")
    public void the_square_area_is(Double result) throws Throwable {
        assertEquals(result, this.result);
    }

    @Then("^the square perimeter is \"([^\"]*)\"$")
    public void the_square_perimeter_is(Double result) throws Throwable {
        assertEquals(result, this.result);;
    }
}