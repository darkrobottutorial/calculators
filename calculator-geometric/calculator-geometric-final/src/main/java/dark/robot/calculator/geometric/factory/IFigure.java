package dark.robot.calculator.geometric.factory;

import java.util.Map;

public interface IFigure {

    Double getArea(Map<String, Double> arguments);

    Double getPerimeter(Map<String, Double> arguments);
}