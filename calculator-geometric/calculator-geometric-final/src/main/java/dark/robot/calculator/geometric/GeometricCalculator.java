package dark.robot.calculator.geometric;

import dark.robot.calculator.AbstractCalculator;
import dark.robot.calculator.geometric.factory.GeometricCalculatorFactory;
import dark.robot.calculator.geometric.factory.IFigure;
import dark.robot.validator.IValidator;
import dark.robot.validator.Validator;

import java.util.Map;

public class GeometricCalculator extends AbstractCalculator implements IGeometricCalculator {

    private static GeometricCalculator calculator = null;
    private GeometricCalculatorFactory factory;
    private IValidator validator;
    private GeometricCalculator() {
        factory = GeometricCalculatorFactory.getInstance();
        validator = Validator.getInstance();
    }

    public static GeometricCalculator getInstance() {
        GeometricCalculator instance = calculator;

        if (instance == null) {
            instance = new GeometricCalculator();
        }

        return instance;
    }

    @Override
    public Double calculateArea(String figureType, Map<String, Double> arguments) {
        validator.validate(figureType, arguments);

        IFigure figure = factory.createFigure(figureType);

        return figure.getArea(arguments);
    }

    @Override
    public Double calculatePerimeter(String figureType, Map<String, Double> arguments) {
        validator.validate(figureType, arguments);

        IFigure figure = factory.createFigure(figureType);

        return figure.getPerimeter(arguments);
    }
}