package dark.robot.calculator.geometric.figure;

public class FigureNotExistsException extends RuntimeException {

    public static final String DEFAULT_MESSAGE = "Figure with name %s not exists";

    public FigureNotExistsException(String message) {
        super(message);
    }
}