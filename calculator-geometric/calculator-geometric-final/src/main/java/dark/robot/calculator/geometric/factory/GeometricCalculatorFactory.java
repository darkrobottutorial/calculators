package dark.robot.calculator.geometric.factory;

import dark.robot.calculator.geometric.figure.Circle;
import dark.robot.calculator.geometric.figure.Figures;
import dark.robot.calculator.geometric.figure.Rectangle;
import dark.robot.calculator.geometric.figure.Square;

public class GeometricCalculatorFactory implements BaseGeometricCalculatorFactory {

    private static GeometricCalculatorFactory factory = null;

    private GeometricCalculatorFactory() {
        super();
    }

    public static GeometricCalculatorFactory getInstance() {
        GeometricCalculatorFactory instance = factory;

        if (instance == null) {
            instance = new GeometricCalculatorFactory();
        }

        return instance;
    }

    @Override
    public IFigure createFigure(String type) {
        IFigure figure = null;

        switch (Figures.get(type)) {
            case Square:
                figure = Square.getInstance();
                break;
            case Rectangle:
                figure = Rectangle.getInstance();
                break;
            case Circle:
                figure = Circle.getInstance();
                break;
        }

        return figure;
    }
}